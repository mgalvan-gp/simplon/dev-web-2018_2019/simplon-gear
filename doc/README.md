
Pour copier le projet :
* Commande composer install
* Créer .env.local avec données BDD
* Créer base de donnée avec doctrine:database:create
* Le fichier migrations est déjà créé, un simple bin/console doctrine:migrations:migrate
* Pour charger les fixtures : bin/console doctrine:fixtures:load

Consigne initiales : https://gitlab.com/simplonlyon/P9/projets/e-commerce/

Bootswatch utilisé: https://bootswatch.com/simplex/

Wireframe : 

* Page d'accueil : https://wireframe.cc/uCNDSx
* Page d'articles : https://wireframe.cc/gVXgIv
* Page d'un article : https://wireframe.cc/R6LDbu