<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\User;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;

class AdminControllerTest extends WebTestCase
{
    /**
     * @var User
     */
    private $userAdmin;

    /**
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();

        $repoUser = $this->client->getContainer()->get('doctrine')->getManager()->getRepository(User::class);

        $this->userAdmin = $repoUser->findOneById(3);

        $this->assertSame(['ROLE_ADMIN'], $this->userAdmin->getRoles());
    }

    public function testAdminPanelRoute()
    {
        $this->logInAdmin();

        $this->client->request('GET', '/admin');
        $this->assertSame(['ROLE_ADMIN'], $this->userAdmin->getRoles());

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        // $this->client->getResponse()->getContent();

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    public function testAuthUserAdmin()
    {
        $this->logInAdmin();
        $this->client->request('GET', '/admin');

        $this->assertSame(['ROLE_ADMIN'], $this->userAdmin->getRoles());

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function logInAdmin()
    {
        $session = $this->client->getContainer()->get('session');
        $this->assertSame(['ROLE_ADMIN'], $this->userAdmin->getRoles());

        $token = new UsernamePasswordToken($this->userAdmin, $this->userAdmin->getPassword(), 'main', $this->userAdmin->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $this->client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }
}
