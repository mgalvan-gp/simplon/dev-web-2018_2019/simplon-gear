<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SelectedOptionRepository")
 */
class SelectedOption
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $choice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Options", inversedBy="selectedOptions")
     */
    private $originOption;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CartArticle", inversedBy="selectedOptions")
     */
    private $cartArticle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getChoice(): ?string
    {
        return $this->choice;
    }

    public function setChoice(string $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    public function getOriginOption(): ?Options
    {
        return $this->originOption;
    }

    public function setOriginOption(?Options $originOption): self
    {
        $this->originOption = $originOption;

        return $this;
    }

    public function getCartArticle(): ?CartArticle
    {
        return $this->cartArticle;
    }

    public function setCartArticle(?CartArticle $cartArticle): self
    {
        $this->cartArticle = $cartArticle;

        return $this;
    }
}
