<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190404130334 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE owner (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, photo VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, article_id INT NOT NULL, message VARCHAR(255) NOT NULL, date DATETIME NOT NULL, note INT DEFAULT NULL, INDEX IDX_9474526CA76ED395 (user_id), INDEX IDX_9474526C7294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE validated_cart (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, date DATETIME NOT NULL, INDEX IDX_77F384E3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cart_article (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, user_id INT DEFAULT NULL, user_cart_id INT DEFAULT NULL, linked_validated_cart_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, description LONGTEXT DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, quantity INT NOT NULL, INDEX IDX_F9E0C6617294869C (article_id), INDEX IDX_F9E0C661A76ED395 (user_id), INDEX IDX_F9E0C66142D8D3B5 (user_cart_id), INDEX IDX_F9E0C66173E9654A (linked_validated_cart_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE selected_option (id INT AUTO_INCREMENT NOT NULL, origin_option_id INT NOT NULL, cart_article_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, choice VARCHAR(255) NOT NULL, INDEX IDX_A62CADF71A4A5919 (origin_option_id), INDEX IDX_A62CADF712FEF66E (cart_article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE options (id INT AUTO_INCREMENT NOT NULL, article_id INT NOT NULL, label VARCHAR(255) NOT NULL, choice LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_D035FA877294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, description LONGTEXT NOT NULL, photo VARCHAR(255) NOT NULL, stock INT NOT NULL, INDEX IDX_23A0E6612469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_owner (article_id INT NOT NULL, owner_id INT NOT NULL, INDEX IDX_B8D7946D7294869C (article_id), INDEX IDX_B8D7946D7E3C61F9 (owner_id), PRIMARY KEY(article_id, owner_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE validated_cart ADD CONSTRAINT FK_77F384E3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cart_article ADD CONSTRAINT FK_F9E0C6617294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE cart_article ADD CONSTRAINT FK_F9E0C661A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cart_article ADD CONSTRAINT FK_F9E0C66142D8D3B5 FOREIGN KEY (user_cart_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cart_article ADD CONSTRAINT FK_F9E0C66173E9654A FOREIGN KEY (linked_validated_cart_id) REFERENCES validated_cart (id)');
        $this->addSql('ALTER TABLE selected_option ADD CONSTRAINT FK_A62CADF71A4A5919 FOREIGN KEY (origin_option_id) REFERENCES options (id)');
        $this->addSql('ALTER TABLE selected_option ADD CONSTRAINT FK_A62CADF712FEF66E FOREIGN KEY (cart_article_id) REFERENCES cart_article (id)');
        $this->addSql('ALTER TABLE options ADD CONSTRAINT FK_D035FA877294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6612469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE article_owner ADD CONSTRAINT FK_B8D7946D7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_owner ADD CONSTRAINT FK_B8D7946D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES owner (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA76ED395');
        $this->addSql('ALTER TABLE validated_cart DROP FOREIGN KEY FK_77F384E3A76ED395');
        $this->addSql('ALTER TABLE cart_article DROP FOREIGN KEY FK_F9E0C661A76ED395');
        $this->addSql('ALTER TABLE cart_article DROP FOREIGN KEY FK_F9E0C66142D8D3B5');
        $this->addSql('ALTER TABLE article_owner DROP FOREIGN KEY FK_B8D7946D7E3C61F9');
        $this->addSql('ALTER TABLE cart_article DROP FOREIGN KEY FK_F9E0C66173E9654A');
        $this->addSql('ALTER TABLE selected_option DROP FOREIGN KEY FK_A62CADF712FEF66E');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6612469DE2');
        $this->addSql('ALTER TABLE selected_option DROP FOREIGN KEY FK_A62CADF71A4A5919');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C7294869C');
        $this->addSql('ALTER TABLE cart_article DROP FOREIGN KEY FK_F9E0C6617294869C');
        $this->addSql('ALTER TABLE options DROP FOREIGN KEY FK_D035FA877294869C');
        $this->addSql('ALTER TABLE article_owner DROP FOREIGN KEY FK_B8D7946D7294869C');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE owner');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE validated_cart');
        $this->addSql('DROP TABLE cart_article');
        $this->addSql('DROP TABLE selected_option');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE options');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_owner');
    }
}
