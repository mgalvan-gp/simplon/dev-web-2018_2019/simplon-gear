<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Entity\CartArticle;
use App\Entity\SelectedOption;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\CartArticleRepository;
use App\Entity\ValidatedCart;
use App\Entity\User;

class CartController extends AbstractController
{
    /**
     * @Route("/account/cart/{article}", name="cart")
     */
    public function addToCart(Request $request, Article $article, ObjectManager $manager, CartArticleRepository $cartArticleRepository)
    {
        $tempOptions = [];
        foreach ($article->getOptions() as $key => $option) {
            $selectedOption = new SelectedOption();
            $selectedOption->setLabel($option->getLabel());
            $selectedOption->setOriginOption($option);
            $selectedOption->setChoice($request->get($option->getLabel()));
            $tempOptions[] = $selectedOption;
        }

        $matchCartArticle = $cartArticleRepository->findOneBy(['article' => $article, 'userCart' => $this->getUser()]);
        if ($matchCartArticle != null) {
            if ($matchCartArticle->getSelectedOptions()->toArray() == $tempOptions) {
                $matchCartArticle->setQuantity($matchCartArticle->getQuantity() + intval($request->get('quantity')));
                $manager->persist($matchCartArticle);
                if ($request->get('cart') == "cart") {
                    $matchCartArticle->setUserCart($this->getUser());
                } elseif ($request->get('wish') == "wish") {
                    $matchCartArticle->setUser($this->getUser());
                }
                $manager->flush();
                return $this->redirectToRoute('article', [
                    'article' => $article->getId()
                ]);
            }
        }
        $cartArticle = new CartArticle();
        if ($tempOptions !== []) {
            foreach ($option as $tempOptions) {
                $cartArticle->addSelectedOption(($option));
                $manager->persist($option);
            }
        }
        $cartArticle->setName($article->getName());
        $cartArticle->setPrice($article->getPrice());
        $cartArticle->setDescription($article->getDescription());
        $cartArticle->setphoto($article->getphoto());
        $cartArticle->setArticle($article);
        $cartArticle->setQuantity($request->get('quantity'));
        $manager->persist($cartArticle);
        if ($request->get('cart') == "cart") {
            $this->getUser()->addCartArticle($cartArticle);
            $cartArticle->setUserCart($this->getUser());
            $this->addFlash("success", "Ajouté au panier !");
        } elseif ($request->get('wish') == "wish") {
            $this->getUser()->addWishArticle($cartArticle);
            $cartArticle->setUser($this->getUser());
            $this->addFlash("success", "Ajouté à la wishlist !");
        } else {
            $this->addFlash("error", "Erreur");
        }
        $manager->flush();
        return $this->redirectToRoute('article', [
            'article' => $article->getId()
        ]);
    }
    /**
     * @Route("/account/display-cart", name="display_cart")
     */
    public function displayCart()
    {
        return $this->render('cart/display-cart.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/account/remove/{article}", name="remove_article")
     */
    public function removeWishArticle(CartArticle $article, ObjectManager $object)
    {
        $object->remove($article);
        $object->flush();

        return $this->redirectToRoute('user');
    }

    /**
     * @Route("/account/remove/cart/{article}", name="remove_cart_article")
     */
    public function removeArticle(CartArticle $article, ObjectManager $object)
    {
        $object->remove($article);
        $object->flush();

        return $this->redirectToRoute('display_cart');
    }

    /**
     * @Route ("/account/validate-cart/", name="validate_cart")
     */
    public function validateCart(ObjectManager $objectManager)
    {
        $validatedCart = new ValidatedCart();
        $validatedCart->setDate(new \DateTime());
        $validatedCart->setUser($this->getUser());

        $userCart = $this->getUser()->getCartArticles();
        
        foreach ($userCart as $cartArticle) {
            $validatedCart->addCartArticle($cartArticle);
            $this->getUser()->removeCartArticle($cartArticle);
        }
        $objectManager->persist($validatedCart);
        $objectManager->flush();

        $this->addFlash("success", "Commande validée avec succès !");

        return $this->redirectToRoute('user', [
            "valid" => $validatedCart
        ]);
    }
}
