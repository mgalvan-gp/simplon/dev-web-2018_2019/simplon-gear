<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\ModifyType;
use App\Form\ModifyPassType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Article;
use App\Entity\CartArticle;
use App\Entity\ValidatedCart;
use App\Repository\ValidatedCartRepository;

class UserController extends AbstractController
{
    /**
     * @Route("account/user", name="user")
     */
    public function index()
    {
        
        return $this->render('user/user.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("account/modify/{id}", name="modify_profil")
     */
    public function modifyProfil(Request $request, User $user, ObjectManager $object)
    {

        $form = $this->createForm(ModifyType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_USER']);

            $object->persist($user);
            $object->flush();

            return $this->redirectToRoute('user');
        }

        return $this->render('user/modify.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("account/modify-password/{id}", name="modify_password")
     */
    public function modifyPass(Request $request, User $user, ObjectManager $object, UserPasswordEncoderInterface $encoder)
    {

        $form = $this->createForm(ModifyPassType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pass = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($pass);
            $user->setRoles(['ROLE_USER']);

            $object->persist($user);
            $object->flush();

            return $this->redirectToRoute('user');
        }

        return $this->render('user/modify-password.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    // /**
    //  * @Route("/remove/{id}" name="remove_user")
    //  */
    // public function removeUser(User $user, ObjectManager $object)
    // {
    //     $object->remove($user);
    //     $object->flush();

    //     return $this->redirectToRoute('accueil');
    // }

    /**
     * @Route("account/wish-to-cart/{article}", name="wish_to_cart")
     */
    public function wishToCart(CartArticle $article, ObjectManager $objectManager){
        $this->getUser()->removeWishArticle($article);
        $this->getUser()->addCartArticle($article);

        $objectManager->flush();

        return $this->redirectToRoute("user");
    }
}
