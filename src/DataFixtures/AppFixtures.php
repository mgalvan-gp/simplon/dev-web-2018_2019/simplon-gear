<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Owner;
use App\Entity\Article;
use App\Entity\CartArticle;
use App\Entity\Options;
use App\Entity\Comment;
use App\Entity\SelectedOption;
use App\Entity\ValidatedCart;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;
use App\Service\UploadService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class AppFixtures extends Fixture
{
    private $encoder;
    private $uploadService;
    private $faker;
    private $filesystem;

    public function __construct(UserPasswordEncoderInterface $encoder, UploadService $uploadService, Filesystem $filesystem)
    {
        $this->encoder = $encoder;
        $this->uploadService = $uploadService;
        $this->filesystem = $filesystem;
        $this->faker = Faker\Factory::create('fr_FR');
    }
    public function load(ObjectManager $manager)
    {
        // Admin :
        $admin = new User;
        $admin->setAddress("13 Place du pont 75014");
        $admin->setEmail("admin@admin.fr");
        $admin->setName("Admin");
        $admin->setSurname("ADMIN");
        $admin->setPassword($this->encoder->encodePassword($admin, "admin"));
        $admin->setPhoneNumber("06/12/24/12/90");
        $admin->setRoles(["ROLE_ADMIN"]);
        $manager->persist($admin);

        // Categories :
        $vetements = new Category;
        $vetements->setName("Vetements");
        $manager->persist($vetements);

        $accessoires = new Category;
        $accessoires->setName("Accessoires");
        $manager->persist($accessoires);

        $goodies = new Category;
        $goodies->setName("Goodies");
        $manager->persist($goodies);

        // Articles :

            // Mugs :
        $mugPierre = new Article;
        $mugPierre->setCategory($goodies);
        $mugPierre->setName("Mug Pierre");
        $mugPierre->setDescription("Grâce à ce mug, vous serez le meilleur en agilité !");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/mugpierre.jpeg', __DIR__ . '/../../assets/Images/image_mug_pierre.jpeg');
        $imageMugPierre = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_mug_pierre.jpeg'));
        $mugPierre->setPhoto($imageMugPierre);
        $mugPierre->setPrice(25);
        $mugPierre->setStock(20);
        $manager->persist($mugPierre);


        $mugJean = new Article;
        $mugJean->setCategory($goodies);
        $mugJean->setName("Mug Jean");
        $mugJean->setDescription("Après chaques gorgées, votre chevelure se mettra à pousser ! C'est génial !");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/mugjean.jpeg', __DIR__ . '/../../assets/Images/image_mug_jean.jpeg');
        $imageMugJean = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_mug_jean.jpeg'));
        $mugJean->setPhoto($imageMugJean);
        $mugJean->setPrice(25);
        $mugJean->setStock(0);
        $manager->persist($mugJean);
    
        $mugPauline = new Article;
        $mugPauline->setCategory($goodies);
        $mugPauline->setName("Mug Pauline");
        $mugPauline->setDescription("Un mug dotté d'une classe sans nom !");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/mugpauline.jpeg', __DIR__ . '/../../assets/Images/image_mug_pauline.jpeg');
        $imageMugPauline = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_mug_pauline.jpeg'));
        $mugPauline->setPhoto($imageMugPauline);
        $mugPauline->setPrice(25);
        $mugPauline->setStock(10);
        $manager->persist($mugPauline);

            // Tapis :
        $tapisPauline = new Article;
        $tapisPauline->setCategory($goodies);
        $tapisPauline->setName("Tapis de souris Pauline");
        $tapisPauline->setDescription("Rien n'est plus utile !");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/tapis_pauline.jpeg', __DIR__ . '/../../assets/Images/image_tapis_pauline.jpeg');
        $imageTapisPauline = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_tapis_pauline.jpeg'));
        $tapisPauline->setPhoto($imageTapisPauline);
        $tapisPauline->setPrice(15);
        $tapisPauline->setStock(30);
        $manager->persist($tapisPauline);

        $tapisPierre = new Article;
        $tapisPierre->setCategory($goodies);
        $tapisPierre->setName("Tapis de souris Pierre");
        $tapisPierre->setDescription("Votre souris ne se portera que mieux !");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/tapis_pierre.jpeg', __DIR__ . '/../../assets/Images/image_tapis_pierre.jpeg');
        $imageTapisPierre = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_tapis_pierre.jpeg'));
        $tapisPierre->setPhoto($imageTapisPierre);
        $tapisPierre->setPrice(15);
        $tapisPierre->setStock(30);
        $manager->persist($tapisPierre);

        $tapisJean = new Article;
        $tapisJean->setCategory($goodies);
        $tapisJean->setName("Tapis de souris Jean");
        $tapisJean->setDescription("A nous la programmation !!!");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/tapis_jean.jpeg', __DIR__ . '/../../assets/Images/image_tapis_jean.jpeg');
        $imageTapisJean = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_tapis_jean.jpeg'));
        $tapisJean->setPhoto($imageTapisJean);
        $tapisJean->setPrice(15);
        $tapisJean->setStock(30);
        $manager->persist($tapisJean);

            // T-shirts :
        $tshirtPauline = new Article;
        $tshirtPauline->setCategory($goodies);
        $tshirtPauline->setName("T-shirt Pauline");
        $tshirtPauline->setDescription("Il est temps de vous sentir beau.");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/tshirt_pauline.jpeg', __DIR__ . '/../../assets/Images/image_tshirt_pauline.jpeg');
        $imageTshirtPauline = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_tshirt_pauline.jpeg'));
        $tshirtPauline->setPhoto($imageTshirtPauline);
        $tshirtPauline->setPrice(18);
        $tshirtPauline->setStock(20);
        $manager->persist($tshirtPauline);

        $tshirtPierre = new Article;
        $tshirtPierre->setCategory($goodies);
        $tshirtPierre->setName("T-shirt Pierre");
        $tshirtPierre->setDescription("Il est temps de vous sentir beau.");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/tshirt_pierre.jpeg', __DIR__ . '/../../assets/Images/image_tshirt_pierre.jpeg');
        $imageTshirtPierre = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_tshirt_pierre.jpeg'));
        $tshirtPierre->setPhoto($imageTshirtPierre);
        $tshirtPierre->setPrice(18);
        $tshirtPierre->setStock(20);
        $manager->persist($tshirtPierre);;

        $tshirtJean = new Article;
        $tshirtJean->setCategory($goodies);
        $tshirtJean->setName("T-shirt Jean");
        $tshirtJean->setDescription("Il est temps de vous sentir beau.");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/tshirt_jean.jpeg', __DIR__ . '/../../assets/Images/image_tshirt_jean.jpeg');
        $imageTshirtJean = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/image_tshirt_jean.jpeg'));
        $tshirtJean->setPhoto($imageTshirtJean);
        $tshirtJean->setPrice(18);
        $tshirtJean->setStock(20);
        $manager->persist($tshirtJean);

        $chiffon = new Article;
        $chiffon->setCategory($accessoires);
        $chiffon->setName("Un chiffon rouge");
        $chiffon->setDescription("Un super chiffon, parfait pour effacer tableaux blancs comme chagrins d'amour");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/chiffon.jpeg', __DIR__ . '/../../assets/Images/chiffon_rouge.jpeg');
        $imageChiffon = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/chiffon_rouge.jpeg'));
        $chiffon->setPhoto($imageChiffon);
        $chiffon->setPrice(15);
        // Selected option is already old
        $chiffon->setStock(0);
        $manager->persist($chiffon);

        // Chiffon options :
        $option2 = new Options;
        $option2->setChoice(["Propre", "Sale"]);
        $option2->setLabel("Etat");
        $option2->setArticle($chiffon);
        $manager->persist($option2);

        $plante = new Article;
        $plante->setCategory($accessoires);
        $plante->setDescription("Une superbe plante verte qui attend patiemment votre retour du travail en regardant par la fenêtre.");
        $plante->setName("Plante Verte");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/plante.jpeg', __DIR__ . '/../../assets/Images/plante_verte.jpeg');
        $imagePlante = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/plante_verte.jpeg'));
        $plante->setPhoto($imagePlante);
        $plante->setPrice(39);
        $plante->setStock(100);
        $manager->persist($plante);

        $teeshirt = new Article;
        $teeshirt->setCategory($vetements);
        $teeshirt->setDescription("Le seul, l'unique, l'indémodable tee-shirt uni.");
        $teeshirt->setName("Tee-shirt Uni");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/tshirt.jpeg', __DIR__ . '/../../assets/Images/tshirt_uni.jpeg');
        $imageTshirt = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/tshirt_uni.jpeg'));
        $teeshirt->setPhoto($imageTshirt);
        $teeshirt->setPrice(25);
        $teeshirt->setStock(150);
        $manager->persist($teeshirt);

        $gourde = new Article;
        $gourde->setCategory($accessoires);
        $gourde->setDescription("Une réplique de la gourde de Jean, la légende raconte qu'elle se remplit toute seule.");
        $gourde->setName("gourde");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/gourde.jpeg', __DIR__ . '/../../assets/Images/gourde_jean.jpeg');
        $imageGourde = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/gourde_jean.jpeg'));
        $gourde->setPhoto($imageGourde);
        $gourde->setPrice(37);
        $gourde->setStock(0);
        $manager->persist($gourde);

        $rasp = new Article;
        $rasp->setCategory($accessoires);
        $rasp->setDescription("Célèbre Raspberry pi 3 !!");
        $rasp->setName("Raspberry pi 3");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/raspberry.jpeg', __DIR__ . '/../../assets/Images/raspberry_pi.jpeg');
        $imageRasp = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/raspberry_pi.jpeg'));
        $rasp->setPhoto($imageRasp);
        $rasp->setPrice(50);
        $rasp->setStock(0);
        $manager->persist($rasp);

        $glass = new Article;
        $glass->setCategory($accessoires);
        $glass->setDescription("Réplique des lunettes de Pierre, La légende raconte que la solution a un code beugué apparait dedans.");
        $glass->setName("Lunette");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/lunettes.jpeg', __DIR__ . '/../../assets/Images/lunettes_pierre.jpeg');
        $imageGlass = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/lunettes_pierre.jpeg'));
        $glass->setPhoto($imageGlass);
        $glass->setPrice(15);
        $glass->setStock(0);
        $manager->persist($glass);

        $bag = new Article;
        $bag->setCategory($accessoires);
        $bag->setDescription("Réplique du sac de Pauline, à les mêmes propriété que le sac de Mary Poppin's selon la légende.");
        $bag->setName("sac a main");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/sac.jpeg', __DIR__ . '/../../assets/Images/sac_pauline.jpeg');
        $imageBag = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/sac_pauline.jpeg'));
        $bag->setPhoto($imageBag);
        $bag->setPrice(64);
        $bag->setStock(0);
        $manager->persist($bag);

        $pc = new Article;
        $pc->setCategory($accessoires);
        $pc->setDescription("Réplique du PC de Pierre, la legende raconte qu'il code tout seul.");
        $pc->setName("PC");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/pc.jpeg', __DIR__ . '/../../assets/Images/pc_pierre.jpeg');
        $imagePc = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/pc_pierre.jpeg'));
        $pc->setPhoto($imagePc);
        $pc->setPrice(2753);
        $pc->setStock(0);
        $manager->persist($pc);

        $phone = new Article;
        $phone->setCategory($accessoires);
        $phone->setDescription("Réplique du smartphone de Pauline, vitale si vous voulez lui ressemblez.");
        $phone->setName("smartphone");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/smartphone.jpeg', __DIR__ . '/../../assets/Images/smartphone_pauline.jpeg');
        $imageSmartphone = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/smartphone_pauline.jpeg'));
        $phone->setPhoto($imageSmartphone);
        $phone->setPrice(999);
        $phone->setStock(0);
        $manager->persist($phone);

        $poncho = new Article;
        $poncho->setCategory($accessoires);
        $poncho->setDescription("Réplique du poncho de Pauline, vitale si vous voulez lui ressemblez.");
        $poncho->setName("Poncho");
        $this->filesystem->copy(__DIR__ . '/../../assets/Images/poncho.jpeg', __DIR__ . '/../../assets/Images/poncho_pauline.jpeg');
        $imagePoncho = $this->uploadService->upload(new File(__DIR__ . '/../../assets/Images/poncho_pauline.jpeg'));
        $poncho->setPhoto($imagePoncho);
        $poncho->setPrice(29);
        $poncho->setStock(0);
        $manager->persist($poncho);

        // Owners :
        $pierre = new Owner;
        $pierre->setName("Pierre Brogard");
        $pierre->setDescription("Un Lyonnais pur souche mais qui s'adapte aux environnements les plus complexes. Le chic 
        à la Française.");
        $pierre->setPhoto("https://simplon.co/wp-content/uploads/2019/01/IMG_20190109_144321.jpg");
        $manager->persist($pierre);

        $jean = new Owner;
        $jean->setName("Jean Demel");
        $jean->setDescription("Un metalleux au grand coeur, c'est toujours une bonne référence à avoir ! La rumeur cours 
        qu'il va au HellFest cette année mais on ne sait toujours pas ce qu'il fait après le mercredi, mystère mystère...");
        $jean->setPhoto("https://tse4.mm.bing.net/th?id=OIP.Dqwe7DCfNhtPcNRsFUWllAHaJ4&pid=Api");
        $manager->persist($jean);

        $pauline = new Owner;
        $pauline->setName("Pauline Mathelin-Rouillet");
        $pauline->setDescription("Grace a Pauline nous avons trouvé un mentor, une personne qui a su nous guider dans 
        l’acquisition de nos compétences, dans les décisions à prendre et dans l'éthique professionnelle.");
        $pauline->setPhoto("https://simplon.co/wp-content/uploads/2018/09/IMG_2302.jpg");
        $manager->persist($pauline);

        // Assoc :
        $chiffon->addOwner($pierre);
        $chiffon->addOwner($jean);
        $plante->addOwner($pierre);
        $plante->addOwner($jean);
        $teeshirt->addOwner($jean);
        $gourde->addOwner($jean);
        $rasp->addOwner($pierre);
        $rasp->addOwner($jean);
        $rasp->addOwner($pauline);
        $glass->addOwner($pierre);
        $bag->addOwner($pauline);
        $pc->addOwner($pierre);
        $phone->addOwner($pierre);
        $poncho->addOwner($pauline);
        $mugJean->addOwner($jean);
        $mugPauline->addOwner($pauline);
        $mugPierre->addOwner($pierre);
        $tapisJean->addOwner($jean);
        $tapisPauline->addOwner($pauline);
        $tapisPierre->addOwner($pierre);
        $tshirtJean->addOwner($jean);
        $tshirtPierre->addOwner($pierre);
        $tshirtPauline->addOwner($pauline);

        // Tshirt options :
        $option = new Options;
        $option->setLabel("Couleur");
        $option->setChoice(["Rouge", "Verte", "Bleu", "Noir"]);
        $option->setArticle($teeshirt);
        $manager->persist($option);

        $option3 = new Options;
        $option3->setLabel("Taille");
        $option3->setChoice(["S", "M", "L", "XL"]);
        $option3->setArticle($teeshirt);
        $manager->persist($option3);

        $manager->flush();
    }
}
// class AppFixtures extends Fixture
// {
//     private $encoder;
//     private $uploadService;
//     private $faker;
//     private $filesystem;

//     public function __construct(UserPasswordEncoderInterface $encoder, UploadService $uploadService, Filesystem $filesystem)
//     {
//         $this->encoder = $encoder;
//         $this->uploadService = $uploadService;
//         $this->filesystem = $filesystem;
//         $this->faker = Faker\Factory::create('fr_FR');
//     }
//     public function load(ObjectManager $manager)
//     {
//         $jeanmichel = new User;
//         $jeanmichel->setAddress("200 Rue de la madeleine - 69001 Madeleine-Les-Bains");
//         $jeanmichel->setEmail("jeanmichel-lebogoss@hotmail.fr");
//         $jeanmichel->setName("Jean-Michel");
//         $jeanmichel->setSurname("Dufour");
//         $jeanmichel->setPassword($this->encoder->encodePassword($jeanmichel, "94Clachampionsleague"));
//         $jeanmichel->setPhoneNumber("06.12.24.56.90");
//         $jeanmichel->setRoles(["ROLE_ADMIN"]);
//         $manager->persist($jeanmichel);

//         $thomas = new User;
//         $thomas->setAddress("13 Place du pont 75014");
//         $thomas->setEmail("thomas-contactpro@apple.fr");
//         $thomas->setName("Thomas");
//         $thomas->setSurname("SEBASTIANELLI");
//         $thomas->setPassword($this->encoder->encodePassword($thomas, "12345"));
//         $thomas->setPhoneNumber("06/12/24/12/90");
//         $thomas->setRoles(["ROLE_USER"]);
//         $manager->persist($thomas);

//         $admin = new User;
//         $admin->setAddress("13 Place du pont 75014");
//         $admin->setEmail("admin@admin.fr");
//         $admin->setName("Admin");
//         $admin->setSurname("ADMIN");
//         $admin->setPassword($this->encoder->encodePassword($admin, "admin"));
//         $admin->setPhoneNumber("06/12/24/12/90");
//         $admin->setRoles(["ROLE_ADMIN"]);
//         $manager->persist($admin);

//         $vetements = new Category;
//         $vetements->setName("Vetements");
//         $manager->persist($vetements);

//         $accessoires = new Category;
//         $accessoires->setName("Accessoires");
//         $manager->persist($accessoires);

//         $goodies = new Category;
//         $goodies->setName("Goodies");
//         $manager->persist($goodies);

//         $chiffon = new Article;
//         $chiffon->setCategory($accessoires);
//         $chiffon->setName("Un chiffon rouge");
//         $chiffon->setDescription("Un super chiffon, parfait pour effacer tableaux blancs comme chagrins d'amour");
//         $chiffon->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $chiffon->setPrice(15);
//         // Selected option is already old
//         $chiffon->setStock(100);
//         $manager->persist($chiffon);

//         $option2 = new Options;
//         $option2->setChoice(["Propre", "Sale"]);
//         $option2->setLabel("Etat");
//         $option2->setArticle($chiffon);
//         $manager->persist($option2);

//         $plante = new Article;
//         $plante->setCategory($accessoires);
//         $plante->setDescription("Une superbe plante verte qui attend patiemment votre retour du travail en regardant par la fenêtre.");
//         $plante->setName("Plante Verte");
//         $plante->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $plante->setPrice(39);
//         $plante->setStock(9001);
//         $manager->persist($plante);

//         $teeshirt = new Article;
//         $teeshirt->setCategory($vetements);
//         $teeshirt->setDescription("Le seul, l'unique, l'indémodable tee-shirt uni.");
//         $teeshirt->setName("Tee-shirt Uni");
//         $teeshirt->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $teeshirt->setPrice(25);
//         $teeshirt->setStock(2000);
//         $manager->persist($teeshirt);

//         $gourde = new Article;
//         $gourde->setCategory($accessoires);
//         $gourde->setDescription("Une réplique de la gourde de Jean, la légende raconte qu'elle se remplit toute seule.");
//         $gourde->setName("gourde");
//         $gourde->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $gourde->setPrice(37);
//         $gourde->setStock(1500);
//         $manager->persist($gourde);

//         $rasp = new Article;
//         $rasp->setCategory($accessoires);
//         $rasp->setDescription("Célèbre Raspberry pi 3 !!");
//         $rasp->setName("Raspberry pi 3");
//         $rasp->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $rasp->setPrice(50);
//         $rasp->setStock(2356);
//         $manager->persist($rasp);

//         $glass = new Article;
//         $glass->setCategory($accessoires);
//         $glass->setDescription("Réplique des lunettes de Pierre, La légende raconte que la solution a un code beugué apparait dedans.");
//         $glass->setName("Lunette");
//         $glass->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $glass->setPrice(15);
//         $glass->setStock(1643);
//         $manager->persist($glass);

//         $bag = new Article;
//         $bag->setCategory($accessoires);
//         $bag->setDescription("Réplique du sac de Pauline, à les mêmes propriété que le sac de Mary Poppin's selon la légende.");
//         $bag->setName("sac a main");
//         $bag->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $bag->setPrice(64);
//         $bag->setStock(2975);
//         $manager->persist($bag);

//         $pc = new Article;
//         $pc->setCategory($accessoires);
//         $pc->setDescription("Réplique du PC de Pierre, la legende raconte qu'il code tout seul.");
//         $pc->setName("PC");
//         $pc->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $pc->setPrice(2753);
//         $pc->setStock(856);
//         $manager->persist($pc);

//         $phone = new Article;
//         $phone->setCategory($accessoires);
//         $phone->setDescription("Réplique du smartphone de Pauline, vitale si vous voulez lui ressemblez.");
//         $phone->setName("smartphone");
//         $phone->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $phone->setPrice(999);
//         $phone->setStock(649);
//         $manager->persist($phone);

//         $poncho = new Article;
//         $poncho->setCategory($accessoires);
//         $poncho->setDescription("Réplique du poncho de Pauline, vitale si vous voulez lui ressemblez.");
//         $poncho->setName("Poncho");
//         $poncho->setPhoto($this->uploadService->upload(new File($this->faker->image())));
//         $poncho->setPrice(29);
//         $poncho->setStock(6406);
//         $manager->persist($poncho);

//         $pierre = new Owner;
//         $pierre->setName("Pierre Brogard");
//         $pierre->setDescription("Un Lyonnais pur souche mais qui s'adapte aux environnements les plus complexes. Le chic 
//         à la Française.");
//         $pierre->setPhoto("https://simplon.co/wp-content/uploads/2019/01/IMG_20190109_144321.jpg");
//         $manager->persist($pierre);

//         $jean = new Owner;
//         $jean->setName("Jean Demel");
//         $jean->setDescription("Un metalleux au grand coeur, c'est toujours une bonne référence à avoir ! La rumeur cours 
//         qu'il va au HellFest cette année mais on ne sait toujours pas ce qu'il fait après le mercredi, mystère mystère...");
//         $jean->setPhoto("https://tse4.mm.bing.net/th?id=OIP.Dqwe7DCfNhtPcNRsFUWllAHaJ4&pid=Api");
//         $manager->persist($jean);

//         $pauline = new Owner;
//         $pauline->setName("Pauline Mathelin-Rouillet");
//         $pauline->setDescription("Grace a Pauline nous avons trouvé un mentor, une personne qui a su nous guider dans 
//         l’acquisition de nos compétences, dans les décisions à prendre et dans l'éthique professionnelle.");
//         $pauline->setPhoto("https://simplon.co/wp-content/uploads/2018/09/IMG_2302.jpg");
//         $manager->persist($pauline);

//         $plante->addOwner($pierre);
//         $plante->addOwner($jean);
//         $teeshirt->addOwner($jean);
//         $gourde->addOwner($jean);
//         $rasp->addOwner($pierre);
//         $rasp->addOwner($jean);
//         $rasp->addOwner($pauline);
//         $glass->addOwner($pierre);
//         $bag->addOwner($pierre);
//         $pc->addOwner($pierre);
//         $phone->addOwner($pierre);
//         $poncho->addOwner($pauline);

//         $option = new Options;
//         $option->setLabel("Couleur");
//         $option->setChoice(["Rouge", "Verte", "Bleu", "Noir"]);
//         $option->setArticle($teeshirt);
//         $manager->persist($option);

//         $option3 = new Options;
//         $option3->setLabel("Taille");
//         $option3->setChoice(["S", "M", "L", "XL"]);
//         $option3->setArticle($teeshirt);
//         $manager->persist($option3);

//         $cartArticle = new CartArticle;
//         $cartArticle->setArticle($teeshirt);
//         $cartArticle->setDescription($teeshirt->getDescription());
//         $cartArticle->setName($teeshirt->getName());
//         $cartArticle->setPhoto($teeshirt->getPhoto());
//         $cartArticle->setprice($teeshirt->getprice());
//         $cartArticle->setUser($jeanmichel);
//         $cartArticle->setQuantity(3);

//         $cartArticle2 = new CartArticle;
//         $cartArticle2->setArticle($teeshirt);
//         $cartArticle2->setDescription($teeshirt->getDescription());
//         $cartArticle2->setName($teeshirt->getName());
//         $cartArticle2->setPhoto($teeshirt->getPhoto());
//         $cartArticle2->setprice($teeshirt->getprice());
//         $cartArticle2->setUser($thomas);
//         $cartArticle2->setQuantity(1);

//         $selectedOption1 = new SelectedOption;
//         $selectedOption1->setCartArticle($cartArticle);
//         $selectedOption1->setChoice("M");
//         $selectedOption1->setLabel("Taille");
//         $selectedOption1->setOriginOption($option3);
//         $manager->persist($selectedOption1);

//         $selectedOption2 = new SelectedOption;
//         $selectedOption2->setCartArticle($cartArticle);
//         $selectedOption2->setOriginOption($option);
//         $selectedOption2->setChoice("Verte");
//         $selectedOption2->setLabel($option->getLabel());
//         $manager->persist($selectedOption2);

//         $selectedOption3 = new SelectedOption;
//         $selectedOption3->setCartArticle($cartArticle2);
//         $selectedOption3->setOriginOption($option);
//         $selectedOption3->setChoice("Rouge");
//         $selectedOption3->setLabel($option->getLabel());
//         $manager->persist($selectedOption3);

//         $comment1 = new Comment;
//         $comment1->setArticle($teeshirt);
//         $comment1->setMessage("Bravo pour ce travail de qualité !");
//         $comment1->setNote(4);
//         $comment1->setUser($thomas);
//         $comment1->setDate(new \DateTime('2012-10-05 17:45:54'));
//         $manager->persist($comment1);

//         $comment2 = new Comment;
//         $comment2->setArticle($teeshirt);
//         $comment2->setMessage("nul");
//         $comment2->setNote(1);
//         $comment2->setUser($jeanmichel);
//         $comment2->setDate(new \DateTime());
//         $manager->persist($comment2);

//         $order = new ValidatedCart;
//         $order->setDate(new \DateTime());
//         $order->setUser($thomas);
//         $order->addCartArticle($cartArticle);
//         $order->addCartArticle($cartArticle2);
//         $order->addCartArticle($cartArticle2);
//         $manager->persist($order);

//         $manager->flush();
//     }
// }
